package tzlogin.data;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import tzlogin.modular.BungeeCord;

import java.util.HashMap;

import static tzlogin.TzLogin.getmain;

public class Config {
    public static int min;//最小输入值
    public static int accountrestriction;//限制注册数量
    public static boolean repeatpassword;//重复密码
    public static boolean registertologin;//注册完成后登录
    public static boolean changetologin;//修改密码后登录
    public static HashMap<String, String> message;//提示消息

    public void get() {
        //Config.yml自动创建
        getmain().saveDefaultConfig();
        all();
        getmain().getLogger().info(ChatColor.GREEN + "已加载 config 配置");
    }

    public static void reload(CommandSender sender) {
        //重载Config.yml
        getmain().reloadConfig();
        new Config().all();
        sender.sendMessage("[TzLogin] " + ChatColor.GREEN + "已重载 config 配置");
    }

    private void all() {
        FileConfiguration config = getmain().getConfig();
        //获取最小值
        min = config.getInt("Setting.passwordmin");
        //获取限制注册数量
        accountrestriction = config.getInt("Setting.accountrestriction");
        //获取布尔值
        repeatpassword = config.getBoolean("Setting.repeatpassword");
        registertologin = config.getBoolean("Setting.registertologin");
        changetologin = config.getBoolean("Setting.Delay");
        //获取跨服配置
        BungeeCord.Enabled = config.getBoolean("Setting.BungeeCord.Enabled");
        if (BungeeCord.Enabled) {
            //注册跨服通道
            Bukkit.getMessenger().registerOutgoingPluginChannel(getmain(), "BungeeCord");
            BungeeCord.ServerName = config.getString("Setting.BungeeCord.Server");
            BungeeCord.delay = config.getInt("Setting.BungeeCord.Delay");
        }
        //获取提示消息
        message = new HashMap<>();
        for (String string : config.getConfigurationSection("Message").getKeys(false)) {
            message.put(string, config.getString("Message." + string).replace('&', '§'));
        }
    }
}
