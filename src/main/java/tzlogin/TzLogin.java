package tzlogin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import tzlogin.data.Config;
import tzlogin.data.Layout;
import tzlogin.database.Connect;
import tzlogin.database.Manage;

public final class TzLogin extends JavaPlugin {

    private static TzLogin tzlogin;
    public static boolean newAPI;

    public static TzLogin getmain() {
        return tzlogin;
    }

    @Override
    public void onLoad() {
        tzlogin = this;
    }

    @Override
    public void onEnable() {
        //判断是否是1.13版本以上
        newAPI = getApi();
        //加载Config.yml数据
        new Config().get();
        //加载Layout.yml数据
        new Layout().get();
        //连接数据库
        new Connect().getconfig();
        //注册监听事件
        Bukkit.getPluginManager().registerEvents(new Lis(), this);
        //统计插件使用状况
        new Metrics(this, 17867);
    }

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        //判断玩家是否是OP
        if (sender.isOp()) {
            //判断命令长度
            if (args.length == 0) {
                sender.sendMessage(ChatColor.RED + "=====插件帮助=====");
                sender.sendMessage(ChatColor.AQUA + "/TL delete [玩家名] - 删除玩家账号");
                sender.sendMessage(ChatColor.AQUA + "/TL reload <config;layout> - 重载配置");
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("reload")) {
                    Config.reload(sender);
                    Layout.reload(sender);
                }
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("reload")) {
                    if (args[1].equalsIgnoreCase("config")) {
                        Config.reload(sender);
                    } else if (args[1].equalsIgnoreCase("layout")) {
                        Layout.reload(sender);
                    }
                } else if (args[0].equalsIgnoreCase("delete")) {
                    if (Manage.data.containsKey(args[1])) {
                        Manage.DELETE(args[1]);
                        sender.sendMessage("[TzLogin] " + ChatColor.GREEN + "已删除玩家账号数据");
                    } else sender.sendMessage("[TzLogin] " + ChatColor.RED + "该玩家的账号数据不存在");
                }
            }
        }
        return false;
    }

    private boolean getApi(){
        try {
            Material.valueOf("PLAYER_HEAD");
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}
