package tzlogin.modular;

import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class RandomSkin {
    public static boolean randomskin;//是否开启随机按钮皮肤
    public static String mode;//随机模式
    public static List<HashMap<String, ItemStack>> Skins;//按钮皮肤列表数据

    public static void run(Inventory inventory, HashMap<Integer, String> input) {
        //获取随机整数
        int random = new Random().nextInt(Skins.size());
        String ItemStackName;
        //遍历按钮
        for (int i : input.keySet())
            if (input.get(i).equals("Input")) {
                //获取按钮名字
                ItemStackName = ChatColor.stripColor(inventory.getItem(i).getItemMeta().getDisplayName());
                if (mode.equals("suite")) {
                    inventory.setItem(i, Skins.get(random).get(ItemStackName));
                } else {
                    inventory.setItem(i, Skins.get(new Random().nextInt(Skins.size())).get(ItemStackName));
                }
            }
    }
}
