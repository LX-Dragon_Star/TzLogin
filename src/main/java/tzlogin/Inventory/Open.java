package tzlogin.Inventory;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import tzlogin.Lis;
import tzlogin.data.Config;
import tzlogin.data.Layout;
import tzlogin.modular.Password;
import tzlogin.modular.RandomSkin;

import java.util.HashMap;

import static tzlogin.TzLogin.getmain;

public class Open {
    public enum Name {
        REGISTER, LOGIN, CHANGE
        //注册,登录,修改密码
    }

    public enum State {
        NORMAL, LOGIN, LENGTH, CHANGE, ERROR, REPEAT, ATYPISM
        //正常,登录,长度错误,修改密码,错误,再次输入,密码不一致
    }

    static Attribute attribute;
    private static final HashMap<String, String> playertitle = new HashMap<>();//玩家界面标题（玩家名，标题名）

    //打开界面
    public static void run(Player player, Name InventoryName, State InventoryState) {
        //判断要打开什么界面
        switch (InventoryName) {
            case REGISTER://注册界面
                attribute = Layout.inventorys.get(0);
                switch (InventoryState) {
                    case NORMAL://正常状况
                        Create(player, setTitle(attribute.title, player.getName()), attribute);
                        break;
                    case LOGIN://注册后登录状态
                        Create(player, setTitle(attribute.title + ChatColor.GRAY + (ChatColor.BOLD + " --> " + ChatColor.stripColor(Layout.inventorys.get(1).title)), player.getName()), attribute);
                        break;
                    case LENGTH://长度错误状态
                        Create(player, playertitle.get(player.getName()) + Config.message.get("passwordlength").replace("{0}", String.valueOf(Config.min)), attribute);
                        break;
                    case REPEAT://密码重复状态
                        Create(player, playertitle.get(player.getName()) + Config.message.get("passwordreenter"), attribute);
                        break;
                    case ATYPISM://密码不一致状态
                        Create(player, playertitle.get(player.getName()) + Config.message.get("passwordatypism"), attribute);
                        break;
                }
                break;
            case LOGIN://登录界面
                attribute = Layout.inventorys.get(1);
                switch (InventoryState) {
                    case NORMAL://正常状态
                        Create(player, setTitle(attribute.title, player.getName()), attribute);
                        break;
                    case CHANGE://登录后要修改密码状态
                        Create(player, setTitle(attribute.title + ChatColor.GRAY + (ChatColor.BOLD + " --> " + ChatColor.stripColor(Layout.inventorys.get(2).title)), player.getName()), attribute);
                        break;
                    case ERROR://密码错误状态
                        //判断是否已经显示提示
                        Create(player, playertitle.get(player.getName()) + Config.message.get("passworderror"), attribute);
                        break;
                }
                break;
            case CHANGE://修改密码界面
                attribute = Layout.inventorys.get(2);
                switch (InventoryState) {
                    case NORMAL://正常状态
                        Create(player, setTitle(attribute.title, player.getName()), attribute);
                        break;
                    case LOGIN://修改密码后要登录状态
                        Create(player, setTitle(attribute.title + ChatColor.GRAY + (ChatColor.BOLD + " --> " + ChatColor.stripColor(Layout.inventorys.get(1).title)), player.getName()), attribute);
                        break;
                    case LENGTH://长度错误状态
                        //判断是否已经显示提示
                        Create(player, playertitle.get(player.getName()) + Config.message.get("passwordlength").replace("{0}", String.valueOf(Config.min)), attribute);
                        break;
                }
                break;
        }
    }

    //创建完成后给玩家打开界面
    private static void Create(Player player, String Title, Attribute attribute) {
        //密码显示移除
        Password.PasswordShow.remove(player.getName());
        //创建界面新实例
        Inventory inventory = Bukkit.createInventory(null, 54, Title);
        //设置界面物品
        inventory.setContents(attribute.above);
        //随机按钮皮肤
        if (RandomSkin.randomskin)
            RandomSkin.run(inventory, attribute.input);
        //设置玩家背包物品
        player.getInventory().setContents(attribute.below);
        //打开界面
        Bukkit.getScheduler().runTask(getmain(), () -> {
            //预防重复关闭界面事件
            Lis.login.remove(player.getName());
            player.openInventory(inventory);
            Lis.login.add(player.getName());
        });
    }

    //缓存玩家界面标题
    private static String setTitle(String Title, String PlayerName) {
        playertitle.remove(PlayerName);
        playertitle.put(PlayerName, Title);
        return Title;
    }
}
