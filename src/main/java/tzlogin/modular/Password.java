package tzlogin.modular;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tzlogin.data.Icon;

import java.util.HashSet;
import java.util.Random;

public class Password {
    public static final HashSet<String> PasswordShow = new HashSet<>();//存储密码显示的玩家

    //密码隐藏
    public static void Hide(Player player) {
        PasswordShow.remove(player.getName());
        //遍历第一行物品栏
        for (int i = 0; i < 9; i++) {
            //判断物品是否为空
            if (player.getInventory().getItem(i) != null) {
                ItemStack itemStack;
                //获取隐藏图标
                if (RandomSkin.randomskin) {
                    itemStack = RandomSkin.Skins.get(new Random().nextInt(RandomSkin.Skins.size())).get("?");
                } else itemStack = Icon.itemStacks.get("?");
                ItemMeta itemMeta = itemStack.getItemMeta();
                //设置隐藏图标的名字
                itemMeta.setDisplayName(player.getInventory().getItem(i).getItemMeta().getDisplayName());
                itemStack.setItemMeta(itemMeta);
                player.getInventory().setItem(i, itemStack);
            }
        }
    }

    //密码显示
    public static void Show(Player player) {
        PasswordShow.add(player.getName());
        //遍历第一行物品栏
        for (int i = 0; i < 9; i++) {
            //判断是否为空
            if (player.getInventory().getItem(i) != null) {
                ItemStack itemStack;
                String ItemStackName = ChatColor.stripColor(player.getInventory().getItem(i).getItemMeta().getDisplayName());
                //获取物品名字转换为对应物品图标
                if (RandomSkin.randomskin) {
                    itemStack = RandomSkin.Skins.get(new Random().nextInt(RandomSkin.Skins.size())).get(ItemStackName);
                } else itemStack = Icon.itemStacks.get(ItemStackName);
                player.getInventory().setItem(i, itemStack);
            }
        }
    }

    //属于隐藏密码
    public static void isHide(Inventory inventory, ItemStack currentItem) {
        ItemStack itemStack;
        //获取隐藏图标
        if (RandomSkin.randomskin) {
            itemStack = RandomSkin.Skins.get(new Random().nextInt(RandomSkin.Skins.size())).get("?");
        } else itemStack = Icon.itemStacks.get("?");
        ItemMeta itemMeta = itemStack.getItemMeta();
        //设置隐藏图标名字
        itemMeta.setDisplayName(currentItem.getItemMeta().getDisplayName());
        itemStack.setItemMeta(itemMeta);
        inventory.setItem(inventory.firstEmpty(), itemStack);
    }
}
