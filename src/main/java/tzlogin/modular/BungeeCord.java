package tzlogin.modular;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import tzlogin.TzLogin;
import tzlogin.data.Config;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import static tzlogin.TzLogin.getmain;

public class BungeeCord {
    public static boolean Enabled;//是否启动
    public static String ServerName;//传送的服务器名字
    public static int delay;//延迟

    //跨服传送
    public static void run(Player player) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("Connect");
            out.writeUTF(ServerName);
        } catch (IOException ignored) {
        }
        //发送消息
        player.sendMessage(Config.message.get("transmitserver").replace("{0}", String.valueOf(delay)));
        //延迟传送
        Bukkit.getScheduler().runTaskLater(getmain(), () -> player.sendPluginMessage(TzLogin.getmain(), "BungeeCord", b.toByteArray()), delay * 20L);
    }
}
