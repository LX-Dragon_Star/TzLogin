package tzlogin.data;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tzlogin.TzLogin;
import tzlogin.modular.RandomSkin;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static tzlogin.TzLogin.getmain;

public class Icon {
    public static HashMap<String, ItemStack> itemStacks;//缓存物品图标(键名，物品)
    List<String> Other;
    List<String> Input;

    void get() {
        //新建和重载清空
        itemStacks = new HashMap<>();
        Other = new ArrayList<>();
        Input = new ArrayList<>();
        File dataFile = new File(getmain().getDataFolder(), "icons.yml");
        //检查文件是否存在
        if (!dataFile.exists()) {
            getmain().saveResource("icons.yml", false);
            getmain().saveResource("skins/default.yml", false);
            getmain().saveResource("skins/default2.yml", false);
            getmain().saveResource("skins/default3.yml", false);
        }
        FileConfiguration data = YamlConfiguration.loadConfiguration(dataFile);
        //遍历文件内容
        for (String s : data.getKeys(false)) {
            if (s.equals("Input")) {
                FileConfiguration data2 = YamlConfiguration.loadConfiguration(new File(getmain().getDataFolder(), "skins/" + data.getString(s + ".Skin")));
                //遍历添加至缓存物品图标
                for (String s1 : data2.getKeys(false)) {
                    getHead(data2, s1, itemStacks);
                    Input.add(s1);
                }
                //随机皮肤是否开启
                if (data.getBoolean(s + ".Random.Enabled")) {
                    RandomSkin.randomskin = true;
                    //获取模式
                    RandomSkin.mode = data.getString(s + ".Random.Mode");
                    //新建和清空缓存
                    RandomSkin.Skins = new ArrayList<>();
                    //遍历Input.Random.Skins
                    for (String s1 : data.getString(s + ".Random.Skins").split(";")) {
                        //获取对应文件
                        FileConfiguration data3 = YamlConfiguration.loadConfiguration(new File(getmain().getDataFolder(), "skins/" + s1));
                        HashMap<String, ItemStack> hashMap = new HashMap<>();
                        //遍历文件数据
                        for (String s2 : data3.getKeys(false)) {
                            getHead(data3, s2, hashMap);
                        }
                        RandomSkin.Skins.add(hashMap);
                    }
                } else RandomSkin.randomskin = false;
            } else {
                //遍历添加至缓存物品图标
                for (String s1 : data.getConfigurationSection(s).getKeys(false)) {
                    itemStacks.put(s1, CreateItemStack(data.getString(s + "." + s1 + ".material"), data.getInt(s + "." + s1 + ".data"), data.getString(s + "." + s1 + ".name"), null, null));
                    if (s.equals("Other"))
                        Other.add(s1);
                }
            }
        }
    }

    private void getHead(FileConfiguration data2, String s1, HashMap<String, ItemStack> itemStacks) {
        ItemStack itemStack;
        if (TzLogin.newAPI) {
            itemStack = CreateItemStack("PLAYER_HEAD", 0, s1, data2.getString(s1 + ".color"), data2.getString(s1 + ".url"));
        } else
            itemStack = CreateItemStack("SKULL_ITEM", 3, s1, data2.getString(s1 + ".color"), data2.getString(s1 + ".url"));
        itemStacks.put(s1, itemStack);
    }

    //创建图标物品
    private ItemStack CreateItemStack(String material, int data, String name, String color, String skin) {
        try {
            //创建物品
            ItemStack itemStack = new ItemStack(Material.getMaterial(material), 1, (byte) data);
            if (skin != null)
                //设置按钮皮肤
                HeadFromUrl(itemStack, skin);
            //获取物品数据
            ItemMeta itemMeta = itemStack.getItemMeta();
            //设置物品名字
            if (color != null) {
                itemMeta.setDisplayName(color.replace('&', '§') + name);
            } else itemMeta.setDisplayName(name.replace('&', '§'));
            itemStack.setItemMeta(itemMeta);
            return itemStack;
        } catch (IllegalArgumentException e) {
            getmain().getLogger().info(ChatColor.RED + material + "不存在!");
            Bukkit.shutdown();
            return null;
        }
    }

    private void HeadFromUrl(ItemStack itemStack, String url) {
        String base64 = UrlToBase64(url);
        UUID hashAsId = new UUID(base64.hashCode(), base64.hashCode());
        String s = "{SkullOwner:{Id:\"" + hashAsId + "\",Properties:{textures:[{Value:\"" + base64 + "\"}]}}}";
        Bukkit.getUnsafe().modifyItemStack(itemStack, s);
    }

    private String UrlToBase64(String url) {
        URI actualUrl;
        try {
            actualUrl = new URI(url);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        String toEncode = "{\"textures\":{\"SKIN\":{\"url\":\"" + "http://textures.minecraft.net/texture/" + actualUrl + "\"}}}";
        return Base64.getEncoder().encodeToString(toEncode.getBytes());
    }
}
