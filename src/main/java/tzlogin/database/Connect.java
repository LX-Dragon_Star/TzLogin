package tzlogin.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static tzlogin.TzLogin.getmain;


public class Connect {
    static HikariDataSource hikariDataSource;
    private final FileConfiguration config = getmain().getConfig();

    //连接池配置
    private String driver;
    private String JdbcUrl;

    public void getconfig() {
        if (config.getString("Database.Use").equals("sqlite")) {
            driver = "org.sqlite.JDBC";
            CheckDatabase("sqlite", "jdbc:sqlite:" + getmain().getDataFolder() + "\\database.db");
        } else {
            driver = "com.mysql.jdbc.Driver";
            CheckDatabase("mysql", "jdbc:mysql://" + config.getString("Database.MysqlConfig.address") + "?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC");
        }
        ConnectionDatabase();
        new Manage().getData();
    }

    private void ConnectionDatabase() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(driver);
        hikariConfig.setJdbcUrl(JdbcUrl);
        hikariConfig.setMinimumIdle(config.getInt("Database.HikariCPConfig.minimumIdle"));
        hikariConfig.setMaximumPoolSize(config.getInt("Database.HikariCPConfig.maximumPoolSize"));
        hikariConfig.setUsername(config.getString("Database.MysqlConfig.username"));
        hikariConfig.setPassword(config.getString("Database.MysqlConfig.password"));
        hikariConfig.setConnectionTestQuery(config.getString("Database.HikariCPConfig.ConnectionTestQuery"));
        hikariDataSource = new HikariDataSource(hikariConfig);
    }

    private void CheckDatabase(String UseDatabase, String JdbcUrl) {
        Connection connection;
        try {
            if (UseDatabase.equals("sqlite")) {
                Class.forName("org.sqlite.JDBC");
                connection = DriverManager.getConnection(JdbcUrl);
                connection.createStatement().execute("create table if not exists PlayerPassword (Name text,Password text,IP text)");
                this.JdbcUrl = JdbcUrl;
            } else {
                connection = DriverManager.getConnection(JdbcUrl, config.getString("Database.MysqlConfig.username"), config.getString("Database.MysqlConfig.password"));
                if (!connection.createStatement().executeQuery("show databases like 'TzLogin'").next()) {
                    connection.createStatement().execute("create database TzLogin");
                    connection.createStatement().execute("use TzLogin");
                    connection.createStatement().execute("create table PlayerPassword (Name text,Password text,IP text)");
                }
                this.JdbcUrl = JdbcUrl.replace("?", "/TzLogin?");
            }
            connection.close();
        } catch (SQLException | ClassNotFoundException e) {
            getmain().getLogger().info(ChatColor.RED + "连接数据库失败!");
            Bukkit.shutdown();
        }
    }
}
