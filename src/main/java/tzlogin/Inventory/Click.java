package tzlogin.Inventory;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import tzlogin.Lis;
import tzlogin.data.Config;
import tzlogin.data.Icon;
import tzlogin.data.Layout;
import tzlogin.database.Manage;
import tzlogin.modular.Backpack;
import tzlogin.modular.BungeeCord;
import tzlogin.modular.Password;

import java.util.HashMap;
import java.util.HashSet;

import static tzlogin.database.Manage.data;

public class Click {
    private enum Input {
        INPUT, DELETE, CHANGE, EXIT, REGISTER, LOGIN, SHOW
        //输入、删除、修改密码，退出服务器，注册，登录，显示密码
    }

    private static final HashSet<String> Change_password = new HashSet<>();//要修改密码的玩家
    private static final HashMap<String, String> playerpassword = new HashMap<>();//重复密码（玩家名，第一次密码）
    private static Inventory inventory;
    private static Player player;

    public static void run(String String, InventoryClickEvent event) {
        //获取下界面（玩家背包）
        inventory = event.getView().getBottomInventory();
        //获取点击玩家
        player = (Player) event.getView().getPlayer();
        switch (Input.valueOf(String.toUpperCase())) {
            case INPUT://输入键
                //判断输入长度是否满
                if (inventory.getItem(8) == null)
                    //判断玩家输入状态（隐藏密码或显示密码）
                    if (Password.PasswordShow.contains(player.getName())) {
                        inventory.setItem(inventory.firstEmpty(), event.getCurrentItem());
                    } else Password.isHide(inventory, event.getCurrentItem());
                break;
            case DELETE://删除键
                //判断格子是否为空
                if (inventory.getItem(0) != null) {
                    inventory.setItem(inventory.firstEmpty() - 1, new ItemStack(Material.AIR));
                }
                break;
            case EXIT://退出键
                Lis.login.remove(player.getName());
                Backpack.Display(player);
                player.kickPlayer(Config.message.get("exitserver"));
                break;
            case SHOW://显示密码键
                //判断玩家密码是否已显示
                if (Password.PasswordShow.contains(player.getName())) {
                    //设置显示按钮键
                    event.setCurrentItem(Icon.itemStacks.get("Show"));
                    Password.Hide(player);
                } else {
                    //设置隐藏按钮键
                    event.setCurrentItem(Icon.itemStacks.get("Hide"));
                    Password.Show(player);
                }
                break;
            case REGISTER://注册键
                PasswordCheck(Input.REGISTER);
                break;
            case LOGIN://登录键
                PasswordCheck(Input.LOGIN);
                break;
            case CHANGE://修改密码键
                //判断是在登录界面点击的还是修改密码界面点击的
                if (player.getOpenInventory().getTitle().startsWith(Layout.inventorys.get(1).title)) {
                    //添加玩家进入修改密码列表
                    Change_password.add(player.getName());
                    //打开新界面
                    Open.run(player, Open.Name.LOGIN, Open.State.CHANGE);
                } else PasswordCheck(Input.CHANGE);
                break;
        }
    }

    //密码检查
    private static void PasswordCheck(Input input) {
        //密码
        StringBuilder password = new StringBuilder();
        //获取密码
        for (int i = 0; i <= 8; i++) {
            ItemStack itemStack = inventory.getItem(i);
            if (itemStack != null) {
                password.append(ChatColor.stripColor(itemStack.getItemMeta().getDisplayName()));
            }
        }
        //判断是在什么界面
        switch (input) {
            case REGISTER://注册界面
                //判断注册密码长度
                if (password.length() >= Config.min) {
                    //重复密码
                    if (Config.repeatpassword) {
                        if (!playerpassword.containsKey(player.getName())) {
                            playerpassword.put(player.getName(), password.toString());
                            Open.run(player, Open.Name.REGISTER, Open.State.REPEAT);
                        } else if (playerpassword.get(player.getName()).equals(password.toString())) {
                            StorePassword(password.toString());
                        } else {
                            playerpassword.remove(player.getName());
                            Open.run(player, Open.Name.REGISTER, Open.State.ATYPISM);
                        }
                    } else StorePassword(password.toString());
                } else Open.run(player, Open.Name.REGISTER, Open.State.LENGTH);
                break;
            case LOGIN://登录界面
                //判断输入的密码是否相等
                if (data.get(player.getName()).equals(password.toString())) {
                    //判断是否需要修改密码
                    if (Change_password.contains(player.getName())) {
                        Change_password.remove(player.getName());
                        //判断修改密码后是否要登录
                        if (Config.changetologin) {
                            Open.run(player, Open.Name.CHANGE, Open.State.LOGIN);
                        } else Open.run(player, Open.Name.CHANGE, Open.State.NORMAL);
                    } else {
                        CloseInventory();
                        player.sendMessage(Config.message.get("login"));
                        if (BungeeCord.Enabled)
                            BungeeCord.run(player);
                    }
                } else Open.run(player, Open.Name.LOGIN, Open.State.ERROR);
                break;
            case CHANGE://修改密码界面
                //判断密码长度
                if (password.length() >= Config.min) {
                    Manage.UPDATE(player.getName(), password.toString());
                    //修改密码后是否要登录
                    if (Config.changetologin) {
                        Open.run(player, Open.Name.LOGIN, Open.State.NORMAL);
                    } else {
                        CloseInventory();
                        player.sendMessage(Config.message.get("change"));
                        if (BungeeCord.Enabled)
                            BungeeCord.run(player);
                    }
                } else Open.run(player, Open.Name.CHANGE, Open.State.LENGTH);
                break;
        }
    }

    private static void CloseInventory() {
        Lis.login.remove(player.getName());
        player.closeInventory();
        Backpack.Display(player);
    }

    private static void StorePassword(String password) {
        //存储玩家密码
        Manage.INSERT(player.getName(), password);
        //注册后是否登录
        if (Config.registertologin) {
            Open.run(player, Open.Name.LOGIN, Open.State.NORMAL);
        } else {
            CloseInventory();
            player.sendMessage(Config.message.get("register"));
            if (BungeeCord.Enabled)
                BungeeCord.run(player);
        }
    }
}
