package tzlogin.Inventory;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class Attribute {
    public String title;//标题
    public ItemStack[] above;//箱子界面物品（上界面）
    public ItemStack[] below;//玩家界面物品（下界面）
    public HashMap<Integer, String> input = new HashMap<>();//按钮（按钮真实格数，按钮的功能）

    public Attribute(String title) {
        this.title = title;
    }
}
