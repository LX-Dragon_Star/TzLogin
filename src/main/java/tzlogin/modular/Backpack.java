package tzlogin.modular;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class Backpack {

    private static final HashMap<String, ItemStack[]> PlayerBackpackData = new HashMap<>();//玩家背包数据

    public static void Hide(Player player) {
        //存储玩家背包数据
        PlayerBackpackData.put(player.getName(), player.getInventory().getContents());
    }

    public static void Display(Player player) {
        //设置玩家的背包数据
        player.getInventory().setContents(PlayerBackpackData.get(player.getName()));
        PlayerBackpackData.remove(player.getName());
    }
}
