package tzlogin.data;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import tzlogin.Inventory.Attribute;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static tzlogin.TzLogin.getmain;

public class Layout {
    private static final File dataFile = new File(getmain().getDataFolder(), "layouts.yml");
    private static FileConfiguration data;
    public static List<Attribute> inventorys;//缓存界面属性
    private Attribute attribute;
    private Icon icon;

    public void get() {
        //判断layouts.yml文件是否存在
        if (!dataFile.exists()) {
            getmain().saveResource("layouts.yml", false);
        }
        all();
        getmain().getLogger().info(ChatColor.GREEN + "已加载 layout 配置");
    }

    public static void reload(CommandSender sender) {
        //重载layouts.yml文件
        try {
            data.load(dataFile);
        } catch (IOException | InvalidConfigurationException e) {
            throw new RuntimeException(e);
        }
        new Layout().all();
        sender.sendMessage("[TzLogin] " + ChatColor.GREEN + "已重载 layout 配置");
    }

    private void all() {
        icon = new Icon();
        //获取图标物品
        icon.get();
        //新建和重载清空
        inventorys = new ArrayList<>();
        data = YamlConfiguration.loadConfiguration(dataFile);
        //遍历添加缓存界面属性
        for (String s : data.getKeys(false)) {
            attribute = new Attribute(data.getString(s + ".Title").replace('&', '§'));
            //获取界面上部分物品
            attribute.above = getItemStack(data.getStringList(s + ".Layout.above"));
            //获取界面下部分物品
            attribute.below = getItemStack(data.getStringList(s + ".Layout.below"));
            inventorys.add(attribute);
        }
    }

    private ItemStack[] getItemStack(List<String> list) {
        //判断界面是上面还是下面
        ItemStack[] itemStacks = new ItemStack[list.size() == 6 ? 54 : 35];
        //设置格子
        int i = list.size() == 6 ? 0 : 9;
        //遍历字符串列表
        for (String s : list) {
            //差分字符串
            for (String s1 : s.split("`")) {
                //差分会有空字符，因此需要判断
                if (!s1.isEmpty()) {
                    //判断字符串在缓存物品图标是否存在
                    if (Icon.itemStacks.containsKey(s1)) {
                        getInput(list.size(), i, s1);
                        itemStacks[i++] = Icon.itemStacks.get(s1);
                    } else
                        //不存在就取字节创建图标
                        for (char c : s1.toCharArray()) {
                            String s2 = Character.toString(c);
                            if (Icon.itemStacks.containsKey(s2)) {
                                getInput(list.size(), i, s2);
                                itemStacks[i] = Icon.itemStacks.get(s2);
                            }
                            i++;
                        }
                }
            }
        }
        return itemStacks;
    }

    //获取缓存界面的Input属性
    private void getInput(int size, int i, String s) {
        if (!icon.Other.contains(s)) {
            //判断是上界面还是下界面（因为判断的是真实格数）
            i = size == 6 ? i : i + 45;
            if (icon.Input.contains(s)) {
                attribute.input.put(i, "Input");
            } else attribute.input.put(i, s);
        }
    }
}
