package tzlogin;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.*;
import tzlogin.Inventory.Attribute;
import tzlogin.Inventory.Click;
import tzlogin.Inventory.Open;
import tzlogin.data.Config;
import tzlogin.data.Layout;
import tzlogin.database.Manage;
import tzlogin.modular.Backpack;

import java.util.*;

import static tzlogin.TzLogin.getmain;

public class Lis implements Listener {
    public static final HashSet<String> login = new HashSet<>();//玩家登录列表

    @EventHandler
    //玩家加入服务器事件
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        //隐藏玩家背包
        Backpack.Hide(player);
        //判断玩家是否第一次注册
        if (!Manage.data.containsKey(player.getName())) {
            //限制同IP注册账号数量
            if (Config.accountrestriction == 0 || (Manage.IP.get(Manage.getIP(player.getName())) == null || Manage.IP.get(Manage.getIP(player.getName())).size() < Config.accountrestriction)) {
                //判断注册后是否要登录
                if (Config.registertologin) {
                    Open.run(player, Open.Name.REGISTER, Open.State.LOGIN);
                } else Open.run(player, Open.Name.REGISTER, Open.State.NORMAL);
            } else
                player.kickPlayer(Config.message.get("accountrestriction").replace("{0}", String.valueOf(Config.accountrestriction)));
        } else Open.run(player, Open.Name.LOGIN, Open.State.NORMAL);
    }

    @EventHandler
    //玩家点击界面事件
    public void onInventoryClickEvent(InventoryClickEvent event) {
        if (event.getCurrentItem() != null)
            for (Attribute attribute : Layout.inventorys)
                if (event.getView().getTitle().startsWith(attribute.title)) {
                    if (attribute.input.containsKey(event.getRawSlot()))
                        Click.run(attribute.input.get(event.getRawSlot()), event);
                    event.setCancelled(true);
                    break;
                }
    }

    @EventHandler
    //玩家关闭界面事件
    public void onInventoryCloseEvent(InventoryCloseEvent event) {
        if (event.getInventory() != null && login.contains(event.getPlayer().getName()))
            Bukkit.getScheduler().runTaskAsynchronously(getmain(), () -> event.getPlayer().openInventory(event.getInventory()));
    }

    @EventHandler
    //玩家移动事件
    public void onPlayerMoveEvent(PlayerMoveEvent event) {
        if (login.contains(event.getPlayer().getName()))
            event.setCancelled(true);
    }

    @EventHandler
    //玩家丢弃物品事件
    public void onPlayerDropItemEvent(PlayerDropItemEvent event) {
        if (login.contains(event.getPlayer().getName()))
            event.setCancelled(true);
    }

    @EventHandler
    //玩家捡起物品事件
    public void onEntityPickupItemEvent(PlayerPickupItemEvent event) {
        if (login.contains(event.getPlayer().getName()))
            event.setCancelled(true);
    }

    @EventHandler
    //玩家放置方块事件
    public void onBlockPlaceEvent(BlockPlaceEvent event) {
        if (login.contains(event.getPlayer().getName()))
            event.setCancelled(true);
    }

    @EventHandler
    //玩家破坏方块事件
    public void onBlockBreakEvent(BlockBreakEvent event) {
        if (login.contains(event.getPlayer().getName()))
            event.setCancelled(true);
    }

    @EventHandler
    //玩家交互事件
    public void onPlayerInteractEvent(PlayerInteractEvent event) {
        if (login.contains(event.getPlayer().getName()) && event.getClickedBlock() != null)
            event.setCancelled(true);
    }

    @EventHandler
    //玩家受伤和伤害事件
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        if (login.contains(event.getEntity().getName()) || login.contains(event.getDamager().getName()))
            event.setCancelled(true);
    }

    @EventHandler
    //玩家命令事件
    public void onPlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event){
        if (login.contains(event.getPlayer().getName()))
            event.setCancelled(true);
    }

    @EventHandler
    //玩家聊天事件
    public void onAsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        if (login.contains(event.getPlayer().getName()))
            event.setCancelled(true);
    }
}
